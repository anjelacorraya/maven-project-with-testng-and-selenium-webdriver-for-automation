package MavenProject_01.MavenProject_01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
//import org.testng.annotations.Test;

public class testClassforCheckValidation {
	WebDriver driver = new ChromeDriver();
	
	@BeforeTest
	public void beforetest() throws InterruptedException {
		
		driver.get("https://demo.opencart.com");
		Thread.sleep(3000);
		System.out.println("Before Test working successfully");
		
		
	}
	
	
	@Test(priority = 2)
	public void VerifyLogin() {
		
		driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
		driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys("demodemo@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys("demodemo");
		driver.findElement(By.cssSelector("#content > div > div:nth-child(2) > div > form > input")).click();
		
		String AccountTitle = driver.getTitle();
		System.out.println(AccountTitle);
		
		if (AccountTitle.equals("My Account")) {
			
			System.out.println("Successfully verify Login!!!");
			}
		
		else
			{
			System.out.println("Verify Login Unseccesful !!!");
			}
		
	}
	
	@AfterTest
	
	public void afterTest() {
	String last_title = driver.getTitle();
	System.out.println("The last title page is "+last_title);
	
	if (last_title.equals("Account Logout")) {
		
		System.out.println("User Already Logged out..!!!");
		}
	
	else
		{
		driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
		driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[5]/a")).click();
		System.out.println("User Logout Successfully");
				
		}
	
	driver.close();
		
	}
	
	/*
	 * @AfterTest public void aftertestMethod() { driver.close();
	 * 
	 * System.out.println("After Test working successfully"); }
	 */
	
	@Test(priority = 1)
	//to check the URL validation
	public void pageLoade() throws InterruptedException {
			
		  String j = driver.getTitle();
		  //System.out.println(d);
		  System.out.println("Your page title Is : "+j);
		  //if(driver.getTitle()=="Your Store") //it will work because it comparing the object in java.
		  if( j.equals("Your Store"))//It will work because it comparing with value in java. 
			  
		{
			  System.out.println("Driver open succeesfully..!!!");
			
		}
		else
		{
			  System.out.println("Sorry!! Not Working...!!!");
		}
		    
		
		
	} 
	

	
	
	
}
